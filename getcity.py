import sys
import os
import requests
from datetime import datetime


def main():
    verbose_mode = False
    save_raw_file = False
    save_output = False

    # store the args
    args = sys.argv[1:]
    # check what args are given then enable the relative flag

    if len(args) == 0:
        print(get_help_screen())
        exit(1)

    # print help

    for arg in args:
        if not arg.startswith("-"):
            # catch all "*" <-
            print(f"Invalid arguments : {arg}")
            print(get_help_screen())
            exit(1)
        for c in arg[1:]:
            match c:
                case "h":
                    # Help page
                    print(get_help_screen())
                    exit(0)
                case "v":
                    # Toggle verbose mode
                    verbose_mode = True
                    print("Verbose mode enabled...")
                case "r":
                    # Toggle the save as a raw file
                    save_raw_file = True
                case "s":
                    # Toggle save of the output as a file
                    save_output = True
                case _:
                    # catch all "-*" <-
                    print(f"Invalid arguments : {c}")
                    print(get_help_screen())
                    exit(1)

    # file url, should be a json file
    url_json = """https://www.odwb.be/api/v2/catalog/datasets/postal-codes-belgium/exports/json"""
    # get the file from the given url
    print("Downloading ...")
    req = requests.get(url=url_json)
    # check if the download happened
    if verbose_mode:
        print(req.status_code)
    # if it failed, throw an error
    if req.status_code != 200:
        print("the file is no longer available.")
        exit(1)

    if save_raw_file:
        # save the file in RAW folder
        if not os.path.isdir("./RAW"):
            os.mkdir("./RAW")
        file = open("./RAW/datas.json", "wt")
        file.write(req.content.decode())
        file.close()

    # get the content of the request as a json data stream
    cities = req.json()

    output_str = ""

    for city in cities:
        if city.get("smun_name_fr") is not None:
            if not save_output:
                print(f'{city.get("smun_name_fr")};{city.get("postcode")}')
            output_str += f'{city.get("smun_name_fr")};{city.get("postcode")},\n'
            continue
        if city.get("smun_name_nl") is not None:
            if not save_output:
                print(f'{city.get("smun_name_nl")};{city.get("postcode")}')
            output_str += f'{city.get("smun_name_nl")};{city.get("postcode")},\n'
            continue

    if save_output:
        if not os.path.isdir("./OUTPUT"):
            os.mkdir("./OUTPUT")
        dt = datetime.now()
        dt = dt.strftime('%Y-%m-%d-%H-%M-%S')
        file = open(f"./OUTPUT/output-{dt}.csv", "wt")
        file.write(output_str)
        file.close()
    print("Done!")
    exit(0)


def get_help_screen() -> str:
    return """python getcity.py [args]

    [Description]
            Get infos about Belgian cities from the ODWB's website.
            Generate a list of city names and postcode into a cvs-like format.

    [Args]
    -h      Display this help page
    -r      Save the row json file in the .\RAW folder.
            (Create it if it does not exists)
    -s      Save the output csv as a file in the .\OUTPUT folder.
    -v      Enable verbose mode.
    """


if __name__ == '__main__':
    sys.exit(main())


# Made by MATROX471
# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# For more information, please refer to <http://unlicense.org/>