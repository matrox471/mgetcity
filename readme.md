# Description

Get infos about Belgian cities from the ODWB's website.
Generate a list of city names and postcode into a cvs-like format.

# Requirements
- python
- pip

# Instructions
## Linux
- python -m venv env
- pip install -r ./requirements.txt
- python getcity.py -h

## Windows
In a terminal, do:
- If pip is missing
    - py -m ensurepip --upgrade
- py -m venv env
- Depending on your terminal
    - Powershell :
        - .\env\Scripts\activate.ps1
            (needs to enable script execution)
    - Command Line :
        - .\env\Scripts\activate.bat
- pip install -r ./requirements.txt
- python getcity.py -h

## MacOS

fuck if i know.

Done !